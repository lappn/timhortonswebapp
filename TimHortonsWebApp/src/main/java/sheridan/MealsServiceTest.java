package sheridan;


import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDrinksRegular() {
		MealsService meals = new MealsService();
		List<String> result = meals.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("Drink list is empty", !result.isEmpty());
		
	}
	
	@Test
	public void testDrinksException() {
		MealsService meals = new MealsService();
		List<String> result = meals.getAvailableMealTypes(null);
		assertTrue("Wrong result when null type",result.get(0) == "No Brand Available");
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		MealsService meals = new MealsService();
		List<String> result =meals.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("Result has less than 4 elements",result.size() > 3);
		
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		MealsService meals = new MealsService();
		List<String> result =meals.getAvailableMealTypes(null);
		assertFalse("Result has more than 1 element",result.size() > 1);
		
	}

}
